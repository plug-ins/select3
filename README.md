1.select3支持ajax异步加载数据，并支持分页加载，支持拖动排序。

2.初始化select3涉及到的几个参数说明：
  
  (1)url: url是ajax拉取数据的地址，数据返回json格式，必须包含total_count和items，
   
   其中total_count是数据的总条数（用于下拉列表分页加载），items是返回的数据的数组。
  
  (2)placeholder: placeholder是下拉框的提示语。
  
  (3)query: 是检索的字段
  
  (4)sortable : 是否支持拖动排序，true或者false，默认false
  
  (5)pagesize : pagesize是下拉列表每次加载的数据条数，默认10条。

3.使用方式见example.html.