$.fn.select3 = function (options){
	var opt ={
		element : this,
		url: options.url,
		placeholder: "choose..." || options.placeholder,
		query: options.query,
		sortable : false || options.sortable,
		pagesize : 10 || options.pagesize
	};
	
	/** bind select2 event **/
	var fn = {
		init: function (){
			$(opt.element).select2({
				multiple: true,
				theme:"bootstrap",
				placeholder: opt.placeholder,
				ajax: {
					url: function (params) {
						params.page = params.page || 1;
						var url = opt.url + "?pageNo="+ params.page +"&pagesize=" + opt.pagesize;
						return url;
		            },
		            dataType: 'json',
		            method:"post",
		            delay: 500,
		            data: function(params){
		            	opt.query[opt.query.key] = params.term;
		            	return opt.query;
		            },
				    processResults: function (data, params) {
				    	params.page = params.page || 1;
				    	var result = $.map(data.items, function (obj) {
				    		obj.id = obj.id;
				    		obj.text = obj.id + ' - ' + (obj.text || obj.title || obj.name);
				    		return obj;
				    	});
				      	return {
				      		results: result,
				      		pagination: {
				      			more: (params.page * opt.pagesize) < data.total_count
				      		}
				      	};
				    },
				    cache: true
				  },
				  minimumInputLength: 0,
				  escapeMarkup: function (markup) { return markup; }, 
				  templateResult: function(repo) {
					    if (repo.loading) return repo.text;
					    var str = "<td>" + repo.text + "</td>";
					    var markup = "<table><tr>" + str +"</tr></table>";
					    return markup;
					},
				  templateSelection: function(repo){
					  return repo.text;
				  }
				}
			);
			
			//enable options dragable and sortable
			if(opt.sortable){
				$(opt.element).next().find(".select2-selection__rendered").sortable({
					update: function(event,ui) {
						$(this).children(".select2-selection__choice").each(function(index,element){
							var text = $(element).attr("title");
							$(opt.element).children().each(function(index,element){
								if(text == $(element).text()){
									$(element).clone().attr("selected","selected").appendTo($(opt.element));
									$(element).remove();
								}
							});
						 });
					}
				});
			}
		}
	};
	fn.init();
	return fn;
};